"""Tests the tg cli"""

import functools
import json

import click.testing
import pytest

import tg.cli

PROJECTS = {
    'example': {
        'path': '/example/path',
        'tags': [
            'example'
        ]
    }
}


@pytest.fixture(scope="module")
def runner():
    return click.testing.CliRunner(env={'TG_HOME': '.'})


@pytest.yield_fixture
def invoke(runner, request):
    with runner.isolated_filesystem():
        @functools.wraps(runner.invoke)
        def wrapper(*args):
            result = runner.invoke(tg.cli.main, args, catch_exceptions=False)
            assert not result.exception
            return result
        yield wrapper


@pytest.yield_fixture
def data(runner):
    """Write example PROJECTS to a file, and return a function to read them"""

    with open('data', 'w') as f:
        json.dump(PROJECTS, f)

    with open('data', 'r') as f:
        yield f.read


def test_add(invoke, data):
    invoke('add', '.', '--name', 'PROJECT')
    assert 'PROJECT' in data()


def test_list(invoke, data):
    for name in PROJECTS.keys():
        assert name in invoke('list').output


def test_remove(invoke, data):
    for name in PROJECTS.keys():
        invoke('remove', name)

    for name in PROJECTS.keys():
        assert name not in data()


def test_rename(invoke, data):
    for name in PROJECTS.keys():
        invoke('rename', name, name + '-renamed')

    for name in PROJECTS.keys():
        assert name + '-renamed' in data()
