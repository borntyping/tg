# coding=utf8
"""Command line interface to tg"""

import os.path
import datetime

import click

from tg import __version__
from tg.data import Tg, Project


def checkmark(value):
    if value:
        return click.style('✓', fg='green')
    else:
        return click.style('✗', fg='red')


@click.group()
@click.option(
    '--home', default=click.get_app_dir('tg'), envvar='TG_HOME',
    type=click.Path(
        file_okay=False, resolve_path=True,
        exists=False, readable=True, writable=True),
    help="Path to a directory that tg will store configuration in")
@click.version_option(__version__)
@click.pass_context
def main(ctx, home):
    ctx.obj = Tg.load(home)


@main.command(help="Add a new project")
@click.option('-n', '--name')
@click.option('-t', '--tag', 'tags', multiple=True)
@click.argument('path', type=click.Path(
    file_okay=False, exists=True, readable=True, resolve_path=True))
@click.pass_obj
def add(tg, path, name, tags):
    tg.projects[name or os.path.basename(path)] = Project(path=path, tags=tags)
    tg.save()


@main.command(help="""
    Show commits for the selected projects sorted by time

    Selects commits from the last day by default.
""")
@click.option(
    '--since', metavar='<date>', default='yesterday',
    help="Show commits more recent than a specific date")
@click.option(
    '--until', metavar='<date>',
    help="Show commits older than a specific date")
@click.option('-t', '--tag', help="Show only projects with this tag")
@click.pass_obj
def journal(tg, since, until, tag):
    commits = tg.projects.filter(tag).commits(since=since, until=until)

    for name, commit in commits:
        click.echo("{} {} {} {}".format(
            click.style(str(datetime.datetime.fromtimestamp(
                commit.committed_date)), fg='green', bold=True),
            click.style(commit.name_rev[:7], fg='red'),
            name, commit.summary))


@main.command(help="List all projects and their paths")
@click.option('-t', '--tag', help="Show only projects with this tag")
@click.pass_obj
def list(tg, tag):
    for name, project in tg.projects.filter(tag).display():
        if project.tags:
            click.echo("- {} {} (tags: {})".format(
                name, project.path, ', '.join(project.tags)))
        else:
            click.echo("- {} {}".format(name, project.path))


@main.command(help="Remove a project")
@click.argument('name')
@click.pass_obj
def remove(tg, name):
    del tg.projects[name]
    tg.save()


@main.command(help="Rename a project")
@click.argument('old_name')
@click.argument('new_name')
@click.pass_obj
def rename(tg, old_name, new_name):
    tg.projects[new_name] = tg.projects[old_name]
    del tg.projects[old_name]
    tg.save()


@main.command(help="Show the status of each project")
@click.option('-t', '--tag', help="Show only projects with this tag")
@click.pass_obj
def status(tg, tag):
    for name, project in tg.projects.filter(tag).display():
        statuses = project.statuses()
        click.echo("{} {} {}".format(
            checkmark(not statuses), name, ', '.join(statuses)))
