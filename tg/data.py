"""Serializable state for tg"""

import json
import os.path

import git


class JSONEncoder(json.JSONEncoder):
    def default(self, o):
        if hasattr(o, '__json__') and callable(o.__json__):
            return o.__json__()
        return super(JSONEncoder, self).default(o)


class Project:
    def __init__(self, path, tags):
        self.path = path
        self.tags = tags

    def __repr__(self):
        return "<Project path={!r} tags={!r}>".format(self.path, self.tags)

    def __json__(self):
        return {
            'path': self.path,
            'tags': self.tags
        }

    @property
    def repo(self):
        return git.Repo(self.path)

    def statuses(self):
        statuses = []

        if self.repo.is_dirty():
            statuses.append("git repo is dirty")

        return statuses


class Projects(dict):
    def filter(self, tag=None):
        if tag is None:
            return self
        return self.__class___(
            (n, p) for n, p in self.items() if tag in p.tags)

    def display(self, minimum=10):
        width = max([len(x) for x in self] + [minimum])

        for name in sorted(self):
            yield "{:{}}".format(name, width), self[name]

    def commits(self, **kwargs):
        commits = list()
        for name, project in self.display():
            for commit in project.repo.iter_commits(**kwargs):
                commits.append((name, commit))

        return list(sorted(commits, key=lambda x: x[1].committed_date))


class Tg:
    @staticmethod
    def max_len(values, minimum=0):
        return

    @classmethod
    def load(cls, home):
        home = os.path.abspath(home)
        data_path = os.path.join(home, 'data')

        if os.path.exists(data_path):
            with open(data_path, 'r') as f:
                projects = json.load(f)
        else:
            projects = {}

        return cls({k: Project(**v) for k, v in projects.items()}, home)

    def __init__(self, projects={}, home=None):
        self.projects = Projects(projects)
        self.home = home

    def __json__(self):
        return {'projects': self._projects}

    def save(self):
        if self.home is None:
            raise Exception("No home directory set")

        if not os.path.exists(self.home):
            os.makedirs(self.home)

        with open(os.path.join(self.home, 'data'), 'w') as f:
            pretty = {'indent': 4, 'separators': (',', ': ')}
            json.dump(self.projects, f, cls=JSONEncoder, **pretty)
